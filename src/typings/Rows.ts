import { Row } from './Row';

export type Rows = Array<Row>;
