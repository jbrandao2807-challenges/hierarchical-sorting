export type CategoryNode = {
  name: string;
  value: number | null;
  parent: CategoryNode | null;
  level: number;
  children: CategoryNode[];
};
