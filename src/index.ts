import { exportFile, importFile, parseFile } from './util/fileHandler';
import { hierarchicalSort } from './sort/hierarchicalSort';
import { Row } from './typings/Row';
import { parseResults } from './util/resultsHandler';
import * as path from 'path';

const inputArgs = process.argv.slice(2);

const init = (inputArgs: string[]) => {
  const filePath = inputArgs[0] || path.resolve('./data-sets/data-big-input.txt');
  const importedFile = importFile(filePath);
  const { rows, header } = parseFile(importedFile);
  const sortedSet = hierarchicalSort(rows, (row: Row) => row.net_sales);
  const exportString = parseResults(sortedSet, header);

  exportFile(filePath.replace('input', 'output'), exportString);
};

init(inputArgs);
