import { Rows } from '../typings/Rows';
import { CategoryNode } from '../typings/Category';
import { createNodeHierarchy } from './helpers/NodesUtil';
import { getPropertiesFromRow } from './helpers/RowsUtil';
import { Row } from '../typings/Row';

export const hierarchicalSort = (rows: Rows, getSortValueFn: any) => {
  const propertiesDepth = getPropertiesFromRow(rows[0]).length;
  const allCategories = createNodeHierarchy(rows, getSortValueFn);
  const sortedCategories = sortCategories(allCategories);
  const finalRows = buildRowsOutput(sortedCategories, rows, propertiesDepth);
  return finalRows;
};

const sortCategories = (categories: CategoryNode[]) => {
  const sortedCategories = categories.sort((a, b) => {
    if (a.name === '$total') {
      return -1;
    } else if (b.name === '$total') {
      return 1;
    }
    const valueA = a.value ?? 0;
    const valueB = b.value ?? 0;
    return valueB - valueA;
  });
  categories.forEach((category) => {
    sortCategories(category.children);
  });
  return sortedCategories;
};

const buildRowsOutput = (categories: CategoryNode[], rows: Row[], propertiesDepth: number) => {
  const partialRows = categories.flatMap((category) => {
    return buildRowsFromCategory(category, propertiesDepth);
  });
  const matchedRows = matchRowsToPartials(partialRows, rows);
  return matchedRows;
};

const buildRowsFromCategory = (category: CategoryNode, propertiesDepth: number) => {
  const rows: Array<Partial<Row>> = [];
  const row: Partial<Row> = {};
  row[`property${category.level}`] = category.name;
  rows.push(...getRowsFromChildren(category, row, propertiesDepth));

  return rows;
};

const getRowsFromChildren = (
  category: CategoryNode,
  row: Partial<Row> = {},
  propertiesDepth: number = 0,
): any => {
  return category.children.flatMap((child) => {
    const childRow: Partial<Row> = {
      ...row,
    };
    childRow[`property${child.level}`] = child.name;
    const result = [];
    if (Object.keys(childRow).length === propertiesDepth) {
      result.push(childRow);
    }
    result.push(...getRowsFromChildren(child, childRow, propertiesDepth));

    return result;
  });
};

const matchRowsToPartials = (partialRows: Array<Partial<Row>>, rows: Row[]) => {
  const finalRows: Row[] = [];
  partialRows.forEach((row) => {
    const foundIndex = rows.findIndex((thisRow) => {
      const properties = getPropertiesFromRow(thisRow);
      let isEqual = true;
      for (let i = 0; i < properties.length; i++) {
        isEqual = isEqual && row[properties[i]] === thisRow[properties[i]];
      }
      return isEqual;
    });

    if (foundIndex === -1) {
      throw Error();
    } else {
      finalRows.push(rows[foundIndex]);
    }
  });
  return finalRows;
};
