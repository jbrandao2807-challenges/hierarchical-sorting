import { Row } from '../../typings/Row';

export const getPropertiesFromRow = (row: Row) => {
  return Object.keys(row).filter((value) => value.startsWith('property'));
};
