import { CategoryNode } from '../../typings/Category';
import { Rows } from '../../typings/Rows';
import { getPropertiesFromRow } from './RowsUtil';

export const findNode = (categoryNode: CategoryNode, categoryList: CategoryNode[]) => {
  const foundIndex = categoryList.findIndex(
    (category) => categoryNode.name === category.name && category.level === categoryNode.level,
  );
  if (foundIndex > -1) {
    const thisCategory = categoryList[foundIndex];
    return thisCategory;
  } else {
    return null;
  }
};

export const createNodeHierarchy = (rows: Rows, getSortValueFn: any) => {
  const properties = getPropertiesFromRow(rows[0]);
  const allCategories: CategoryNode[] = [];

  rows.forEach((row) => {
    let parentNode = null;
    for (let i = 0; i < properties.length; i++) {
      const categoryNode: CategoryNode = {
        name: row[properties[i]].toString(),
        level: i,
        parent: null,
        value: null,
        children: [],
      };

      if (i === properties.length - 1) {
        categoryNode.value = getSortValueFn(row);
      }

      if (i === 0) {
        const foundNode = findNode(categoryNode, allCategories);
        if (foundNode === null) {
          allCategories.push(categoryNode);
          parentNode = categoryNode;
        } else {
          categoryNode.parent = foundNode;
          parentNode = foundNode;
        }
      } else {
        if (parentNode === null) {
          throw new Error();
        } else {
          if (categoryNode.name === '$total') {
            const value = getSortValueFn(row);
            parentNode.value = value;
          }
          const foundNode = findNode(categoryNode, parentNode.children);
          if (foundNode === null) {
            parentNode.children.push(categoryNode);
            parentNode = categoryNode;
          } else {
            categoryNode.parent = foundNode;
            parentNode = foundNode;
          }
        }
      }
    }
  });
  return allCategories;
};
