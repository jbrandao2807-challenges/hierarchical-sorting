import { readFileSync, writeFileSync } from 'fs';
import { Rows } from '../typings/Rows';

export const importFile = (filePath: string) => {
  return readFileSync(filePath, { encoding: 'utf-8' });
};

export const exportFile = (filePath: string, data: any) => {
  writeFileSync(filePath, data, { encoding: 'utf-8' });
};

export const parseFile = (rawData: string): { rows: Rows; header: Array<string> } => {
  const parsedData = rawData.split('\n').map((row) => row.split('|'));
  const header = parsedData[0];
  const data = parsedData.slice(1);

  const rows = data.map((row) => {
    const newData: Record<string, string> = {};
    header.forEach((field, index) => {
      newData[field] = row[index];
      if (!field.startsWith('property')) {
        newData[field] = parseFloat(row[index]).toFixed(1).toString();
      }
    });
    newData.rawLine = row.join('|');
    return newData;
  });

  return { rows, header };
};
