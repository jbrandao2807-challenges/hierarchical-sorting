import { Rows } from '../typings/Rows';

export const parseResults = (rows: Rows, header: Array<string>) => {
  return `${header.join('|')}\n${rows.map((row) => row.rawLine).join('\n')}`;
};
