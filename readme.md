# Hierarchical Sorting Challenge

## Source

The source of the challenge is at this url:
https://gist.github.com/surjikal/3c8b21f4ffe92e1af299

## How to run

`npm install` to install dependencies.
`npm run start:big` to run the big data set.
`npm run start:small` to run the small data set.

## Running manually

In order to run other files just use `npx ts-node src/index.ts '<your file path>'`

The project will run the file and output it as long as it contains input in the name.

### Execution Steps

- The application will start by using the argument or default value.
- Then the application will grab the file.
- After reading the file will parse to known format (Row type).
- After parsing it to rows, it will create a hierarchy based on the number of properties that are on the file header.
- By default the application at index.js will grab net_sales but the sorting can be used as a library.
- After sorting the hierarchy, it will rebuild the rows and match each one to the parsed ones.
- After that it will dump the output file.